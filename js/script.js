window.addEventListener("DOMContentLoaded", addTegs);
function addTegs() { 
    const galleryItems = document.querySelectorAll(".work .gallery-item");
    const popUp = document.querySelector(".work .pop-up");
    [...galleryItems].slice(1).forEach(li => li.append(popUp.cloneNode(true)));
}

const searchIcon = document.querySelector(".nav-search-icon");
searchIcon.addEventListener("mouseover", ()=> {
    searchIcon.setAttribute("src" , "./img/header/search-icon-hover.png");
})
searchIcon.addEventListener("mouseout", ()=> {
    searchIcon.setAttribute("src" , "./img/header/search-icon.png");
})

const servicesList = document.querySelector(".services-list");
const servicesDescriptions = document.querySelectorAll(".services-description");

servicesList.addEventListener("click", (evt) => {
    let target = evt.target;
    let prevActive = document.querySelector(".services-list-item.active");
    prevActive.classList.remove("active");
    target.classList.add("active");
    servicesDescriptions.forEach(div => div.dataset.service !== target.dataset.service ?
        div.classList.add("inactive") : div.classList.remove("inactive"))
})

let countDisplayBlock = 12;

const workList = document.querySelector(".work-list");
workList.addEventListener("click", (evt) => {
    const galleryItems = document.querySelectorAll(".work .gallery-item");
    const worksGallery = document.querySelector(".works-gallery")
    let target = evt.target;
    for (let li of workList.children) {
        if (li.matches(".active")) li.classList.remove("active")
    }
    target.classList.add("active");
    let arr = [];                 
    for (let li of galleryItems) {
        li.dataset.typeWork !== target.dataset.typeWork ?
        li.style.display = "none": li.style.display = "block";
        if (target.dataset.typeWork === "all") li.style.display = "block";
        arr.push(window.getComputedStyle(li).display);
    }    
    countDisplayBlock = arr.filter(display => display.includes("block")).length;
    (countDisplayBlock <= 3 || countDisplayBlock % 4 !== 0) ?
    worksGallery.classList.add("works-gallery-js") :
    worksGallery.classList.remove("works-gallery-js");
})

workList.addEventListener("mouseover", (evt) => {
    const galleryImgs = document.querySelectorAll(".work .gallery-img")
    let target = evt.target;
    if (countDisplayBlock >= 12 && target.tagName === "LI" && target.dataset.typeWork !== "all") {
        for (let img of galleryImgs) {
            if (img.dataset.typeWork !== target.dataset.typeWork) 
            img.style.filter = "brightness(25%)";
        }
    }
})
workList.addEventListener("mouseout", (evt) => {
    const galleryImgs = document.querySelectorAll(".work .gallery-img")
    galleryImgs.forEach(img => img.style.filter = "brightness(100%)")
})

const focusLink = document.querySelector(".hidden-link")               
const btnLoad = document.querySelector(".work .load-btn"); 
btnLoad.addEventListener("click", loadImgs)                                   

function loadImgs() {
    btnLoad.classList.remove("bg-btn-add", "bg-btn-min");
    btnLoad.children[0].style.visibility = "visible";            
    const worksGallery = document.querySelector(".works-gallery");       
    let quantLi = worksGallery.children.length;
    setTimeout(() => {                                         
        btnLoad.children[0].style.visibility = "hidden";         
        btnLoad.classList.add("bg-btn-add");                     
        if (quantLi < 25) {                                       
        let factor = quantLi <= 12 ? 0 : 3                                
            for (let i = 1; i <= 12; i += 1) {                  
                let li = worksGallery.children[i-1].cloneNode(true);
                let img = li.children[0];                                 

                if (i <= 3) {                                                             
                    li.setAttribute("data-type-Work", "gd");
                    img.setAttribute("src", `./img/img-base/gd/gd-${i + factor}.jpg`)      
                    img.setAttribute("data-type-Work", "gd");
                } else if (i > 3 && i <= 6) {
                    li.setAttribute("data-type-Work", "wd");
                    img.setAttribute("src", `./img/img-base/wd/wd-${i - 3 + factor}.jpg`) 
                    img.setAttribute("data-type-Work", "wd");
                } else if (i > 6 && i <= 9) {
                    li.setAttribute("data-type-Work", "lp");
                    img.setAttribute("src", `./img/img-base/lp/lp-${i - 6 + factor}.jpg`) 
                    img.setAttribute("data-type-Work", "lp");
                } else if (i > 9 && i <= 12) {
                    li.setAttribute("data-type-Work", "wp");
                    img.setAttribute("src", `./img/img-base/wp/wp-${i - 9 + factor}.jpg`) 
                    img.setAttribute("data-type-Work", "wp");
                }
                worksGallery.append(li);
            }
        }
        if (quantLi >= 24) {                                         
            btnLoad.classList.replace("bg-btn-add", "bg-btn-min");     
            btnLoad.innerHTML = '<i class="loading"></i>MINIMIZE';    
        }
        if (quantLi >= 36) {                                             
            [...worksGallery.children].slice(12).forEach(li => li.remove()) 
            btnLoad.classList.replace("bg-btn-min", "bg-btn-add");       
            btnLoad.innerHTML = '<i class="loading"></i>LOAD MORE'         
            focusLink.click();      
        }
    }, 2000);
}

const usersList = document.querySelector(".users-list");
usersList.addEventListener("click", carouselIcon) 

function carouselIcon(evt) {
    if (evt.target.tagName === "IMG") {
        let target = evt.target
        const usersCards = document.querySelectorAll(".user-feedback");
        let prevActive = document.querySelector(".feedback .user-img.active");
        prevActive.classList.remove("active");
        target.classList.add("active");
        usersCards[0].classList.add("inactive");
        for (let userCard of usersCards) {
            target.dataset.userId !== userCard.dataset.userId ?
                userCard.classList.replace("move", "inactive"):
                userCard.classList.add("move");
        }
    }
}

const btnL = document.querySelector(".feedback button.arrow-left");
const btnR = document.querySelector(".feedback button.arrow-right");
btnL.addEventListener("click", carouselBtnL);
btnR.addEventListener("clcick", carouselBtnR);

function carouselBtnL() {
    const usersCards = document.querySelectorAll(".user-feedback"); 
    const imgs = document.querySelectorAll(".feedback .user-img"); 
    const prevActiveImg = document.querySelector(".feedback .user-img.active"); 
    const prevActiveImgIndex = ([...imgs].indexOf(prevActiveImg)); 
    prevActiveImg.classList.remove("active"); 
    let newActiveImg;           
    if (prevActiveImgIndex === 0) {                        
        newActiveImg = imgs[imgs.length -1];               
        imgs[imgs.length -1].classList.add("active");
    }                                                         
    else {            
        newActiveImg = imgs[prevActiveImgIndex - 1]                
        imgs[prevActiveImgIndex - 1].classList.add("active")  
    }
    usersCards[0].classList.add("inactive");
    for (let userCard of usersCards) {                       
        newActiveImg.dataset.userId !== userCard.dataset.userId ?  
            userCard.classList.replace("move", "inactive"):    
            userCard.classList.add("move");               
    }
}

function carouselBtnR() {
    const usersCards = document.querySelectorAll(".user-feedback");     
    const imgs = document.querySelectorAll(".feedback .user-img");  
    const prevActiveImg = document.querySelector(".feedback .user-img.active");
    const prevActiveImgIndex = ([...imgs].indexOf(prevActiveImg));
    prevActiveImg.classList.remove("active");
    let newActiveImg;
    if (prevActiveImgIndex === imgs.length -1) {
        newActiveImg = imgs[0];
        imgs[0].classList.add("active");
    } else {
        newActiveImg = imgs[prevActiveImgIndex + 1]
        imgs[prevActiveImgIndex + 1].classList.add("active")
    }
    usersCards[0].classList.add("inactive");
    for (let userCard of usersCards) {
        newActiveImg.dataset.userId !== userCard.dataset.userId ?
            userCard.classList.replace("move", "inactive"):
            userCard.classList.add("move");
    }
}